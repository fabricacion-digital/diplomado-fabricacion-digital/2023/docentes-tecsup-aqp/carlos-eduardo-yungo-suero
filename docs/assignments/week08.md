# 6. Computer controlled machining

## Parameterizing router machine.

Is important to know what parameters we need to obtain the best fit between two pieces.
I tried with 18mm MDF. And I need design the holes with 17.6 mm to achieve a good fit.

![](../files/week06/Parameter.jpg)

## Creating a file to router machine

I used "CNC Change" Software to create a file .jg, useful to our cnc router machine.
In this video you can see the process from the design in fusion to create and save the .jg file.

Note: you need to review the bit specifications to set properly your cut.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6b7b3ShiOnU?si=3tzPdEEgrZC-RvNL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## CNC Machining

Now this video shows the process to use cnc router.

<iframe width="560" height="315" src="https://www.youtube.com/embed/leMXOJHgkWI?si=6EpprcpdygukPuFN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

These are some pictures about a litle project.

![](../files/week06/PJ0.jpg)
![](../files/week06/PJ1.jpg)
![](../files/week06/PJ2.jpg)
![](../files/week06/PJ3.jpg)
