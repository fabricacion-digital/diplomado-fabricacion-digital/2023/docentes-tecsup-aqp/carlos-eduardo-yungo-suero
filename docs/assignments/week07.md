# 9. Electronics design

This week I worked on desinning an electronic component

## Selecting the program

The selected program was [flux.ai](https://www.flux.ai/), it is a osftware online with embedded AI.
You just need create an user and after start to create electronics components.

![](../files/week09/1.jpg)

## Troubles and solutions

A common problem is that there aren`t alectronic components in the library, then I can import a new components or use a similar component.

To import a wew component I did the next steps:

I Used [https://www.snapeda.com/](https://www.snapeda.com/) to find the required component.

![](../files/week09/2.jpg)

After finding the component I reviewed if it has the footprint and 3D model.

![](../files/week09/3.jpg)

Then I can see the datsheet and download the Symbol and footprint.

![](../files/week09/4.jpg)

It is important download KiCad format.

![](../files/week09/5.jpg)

With V4 version.

![](../files/week09/6.jpg)

Then I save the footprint and 3D model.

![](../files/week09/7.jpg)
![](../files/week09/8.jpg)
![](../files/week09/9.jpg)

Now I can import the component symbol, footprint and 3Dmodel into Flux.

![](../files/week09/10.jpg)
![](../files/week09/11.jpg)
![](../files/week09/12.jpg)
![](../files/week09/13.jpg)
![](../files/week09/14.jpg)
![](../files/week09/15_16.jpg)
![](../files/week09/17.jpg)
![](../files/week09/18.jpg)
![](../files/week09/19.jpg)
![](../files/week09/20.jpg)
![](../files/week09/21.jpg)
![](../files/week09/22.jpg)
![](../files/week09/23.jpg)
![](../files/week09/24.jpg)
![](../files/week09/25.jpg)
![](../files/week09/26.jpg)
![](../files/week09/27.jpg)
![](../files/week09/28.jpg)
![](../files/week09/29.jpg)
![](../files/week09/30.jpg)
![](../files/week09/31.jpg)
![](../files/week09/32.jpg)

When import the 3D model, it maybe be in wrong position, then I rotated and change the position until the compenent is in a good position.

![](../files/week09/33.jpg)
![](../files/week09/34.jpg)
![](../files/week09/35.jpg)

Finally I published the changes.

![](../files/week09/36.jpg)

To the electronic design I drag and drop each component into Schematic window after linked the pins.

![](../files/week09/37.jpg)

And to the PCB I reoganized the components and ways.

![](../files/week09/39.jpg)
![](../files/week09/38.jpg)

This is the link to the electronic design in flux [click here](https://www.flux.ai/carlosyungo/interesting-apricot-ecto-goggles?editor=schematic)

<iframe height="450" width="800" allowfullscreen src="https://www.flux.ai/carlosyungo/esp32-board?editor=pcb_3d&embed=1" />