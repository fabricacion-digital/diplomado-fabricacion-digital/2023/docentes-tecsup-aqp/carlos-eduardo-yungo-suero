# 1. Principles and practices

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

Key distribution systems in educational centers are essential to ensure security and controlled access to classrooms, offices, and other restricted areas within the institution. These systems are used to manage and control access to different spaces, ensuring that only authorized staff and students have entry.

Here are some highlights and common features of these systems:

Traditional Locks: Some educational centers still use traditional locks and keys to protect their facilities. While effective, they can be inconvenient and challenging to manage if there are a large number of keys and multiple people needing access.

Electronic Locks: More modern and secure systems include electronic locks that can be centrally controlled. These locks can be activated using identification cards, electronic key fobs, or numeric codes.

Card-Based Access Control: Identification cards, such as RFID cards or proximity cards, are a popular option for access control systems. Each card has a unique identification, and access to specific areas is granted based on assigned permissions.

Network-Based Access Control Systems: Some institutions choose to implement network-based access control systems, allowing for easier administration and greater flexibility in granting access permissions. These systems can integrate with other security systems and can be centrally managed.

Time Management: Some key distribution systems are also used to record staff entry and exit times, improving time management and attendance tracking.

Security and Event Logging: Advanced systems may have logging features that track and record access events, enabling the identification of potential security issues or user activity monitoring.

Flexibility and Scalability: It's crucial for key distribution systems in educational centers to be flexible and adaptable to the institution's growth. They should easily expand to include new classrooms or restricted areas as needed.

Staff Training: The successful implementation of a key distribution system requires adequate training for the staff to know how to use and maintain the system properly.

It's essential to emphasize that physical security in schools and educational centers is fundamental to protect students, staff, and the institution's property. Key distribution systems, along with other security measures, can significantly contribute to creating a safe and conducive educational environment for learning.

TECSUP institute has a key distrbution system with tradicional locks, but it is slow in some time when teacher are in a queue.

## Final project
Create a key distribution system when teacher can obtain the key, remote crontrol and pen, typing their ID number, scanning a code or whit fingerprints.

![](../images/week01/draw.jpg)

## What will you do?
This key distribution system will allow TECSUP teacher get the keys in a simple way, typin thier ID number or with other biometric system.
The system record who has the key and delete de record when the key is back.
Is the same to R/C and Pens.

## Who will use it?
It will be used for teachers in TECSUP.