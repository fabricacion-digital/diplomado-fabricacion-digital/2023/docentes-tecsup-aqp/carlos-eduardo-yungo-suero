# 4. Computer controlled cutting

## Using Plotter Cutting Machine

To this assignment I am going to use a 2D design software (corel draw), plotter cutting machine (Roland GX-24) and cut software (cutStudio)

### Starting with a design in Corel

1.- I select the image to the project
![](../files/week04/1_proyect.jpg)

2.- Vectoring with B-spline
![](../files/week04/2_vectoring_with_b_spline.jpg)
![](../files/week04/3_b_spline.jpg)

3.- Using mirror to save time
![](../files/week04/8_mirror.jpg)
![](../files/week04/9_mirror2.jpg)
![](../files/week04/10_mirror_ok.jpg)

4.- Using join to create a whole piece from 2 mirrors.
![](../files/week04/11_soldar.jpg)
![](../files/week04/12_soldar_ok.jpg)

5.- finishing the design
![](../files/week04/13_finish.jpg)
![](../files/week04/14_pieces_to_plotter.jpg)

You can download the design here:
[Download Corel design](../files/week04/DISRUPTOR.cdr)

### Using the plotter cutting machine
It works like a printer, it is necesary install a driver to cut.

1.- Feeding adhesive vinyl and initial settings.

<iframe width="560" height="315" src="https://www.youtube.com/embed/APSi_15Gptk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

2.- Before cut something, it is necesary set the size of adhesive vinil. (there must not be any design uploaded in Cut Studio)

<iframe width="560" height="315" src="https://www.youtube.com/embed/FtHm2iOQcEU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

3.- Now to import the design into CutStudio fron corel, i saved the design in .eps format.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9hjKEPdhInY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

You can download the design here:

[Black; ](../files/week04/DISRUPTORblack.eps)
[White; ](../files/week04/DISRUPTORwhite.eps)
[Red; ](../files/week04/DISRUPTORred.eps)
[Lightblue; ](../files/week04/DISRUPTORlightblue.eps)

4.- You can import this file into Studio cut and cut it.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z7Z-Ch1DzPM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

5.- Finally your cutting plotter machine start to cut.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_wnL28qrU0k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

6.- I'm preparing a piece of acrylic to paste the image.

Bending the acrylic
![](../files/week04/bending_acrylic.jpg)
![](../files/week04/Acrylic_bended.jpg)

7.- This is the final product.

![](../files/week04/final_plotter.jpg)

## Using laser cutting machine

### Starting with a design in Corel

1.- I found an interesting toy to make with cardboard. It is a calculator frog.

[You can download it here.](https://www.instructables.com/Calculating-frog-make-your-own-mechanical-calcula/)

2.- Now I am going to do the design to 3mm mdf.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dGdgfvCPWg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Calculator frog Corel design
[download here](../files/week04/calculator_frog_3mm.cdr)

### Using laser cutting and engraving machine

1.- This machine has some significant systems

Cooling system: to cool the laser tube.
![](../files/week04/cooling_system.jpg)

Compressed air system: to avoid fire when machine is cutting
![](../files/week04/COMPRESSED_AIR_SYSTEM.jpg)

Ventilation system: to extract the smoke produced by cutting process.
![](../files/week04/VENTILATION_SYSTEM.jpg)

2.- Before use the laser machine I need to set the focal lenght, pressing pulse button in several heights to obtain the best laser pulse. it was 5mm.

![](../files/week04/pulse_test.jpg)
![](../files/week04/focal_lenght.jpg)

3.- Now I use a design to obtain the best cutting and fit parameters. This design was made with "RDWorksV8" software.

![](../files/week04/LASER_CUT_PARAMETERS.jpg)
![](../files/week04/back.jpg)

Note: This design was cutted in 4 times.
You can obtain here the .rd files.
[first cut; ](../files/week04/TEST01.rd)
[second cut; ](../files/week04/TEST02.rd)
[third cut; ](../files/week04/TEST03.rd)
[fourth cut](../files/week04/TEST04.rd)

3.- Trying some designs with "RDWorksV8" software.

Part 1

<iframe width="560" height="315" src="https://www.youtube.com/embed/trB1fvvEKqQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Part 2

<iframe width="560" height="315" src="https://www.youtube.com/embed/VHTAYQVVVJ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

To the laser cut machine, you need to save your project in your USB, like uppon videos, after you must copy your design into the machine to follow with the cut proces.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8ieR47MZUuk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

4.- Finally it is the calculator frog.
![](../files/week04/1_frog_cutted.jpg)
![](../files/week04/2_frog_pieces.jpg)
![](../files/week04/3_frog.jpg)
![](../files/week04/4_frog.jpg)
![](../files/week04/5_frog.jpg)

<iframe width="320" height="560" src="https://www.youtube.com/embed/WZGWV4v1yng" title="VID 20230807 155445" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

5.- Also, I tried engraving pens.
![](../files/week04/Trying_pen_engraving.jpg)
![](../files/week04/Trying_engraving2.jpg)


THANK YOU SO MUCH

