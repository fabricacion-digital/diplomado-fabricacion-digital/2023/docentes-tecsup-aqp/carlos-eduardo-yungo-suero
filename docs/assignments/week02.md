# 2. Project management

## Git tutorial

This assignment has the way to link your Pc repository with Gitlab on web.

### 1. Download and install sublime text.

Got to sublime.com and download it
![](../images/week01/1_sublime_page.jpg)

Install subime text (click on next)
![](../images/week01/2_install_sublime.jpg)

### 2. Download and install git to PC

Go to git.com and download git to pc
![](../images/week01/3_git_page.jpg)

Install git
![](../images/week01/4_install_git.jpg)

In this window select Sublime as git's editor
![](../images/week01/4A_select_sublime_into_git_instalation.jpg)

### 3. Create a file in your PC 

It will be your repository

Push right click on your file to open Git bash
![](../images/week01/5_new_folder.jpg)

### 4. Clone your Project on Git Lab to PC 

Got to git lab, and in your project click on clone, after click on copy icon (Clone with HTTPS) and paste in git bash of your PC's project file.
![](../images/week01/6_clone_folder.jpg)

### 5. Create a Password 

In git bash create a password with next command
ssh-keygen -t rsa -b 2048 -C "your email user"
Enter your password twice, Note "you can't see the password typed", after a image password will be generated.
![](../images/week01/7_creating_password.jpg)

Got to edit profile on gitlab
![](../images/week01/8_edit_profile.jpg)

Click on SSH Keys, after click on Add new keys
![](../images/week01/9_add_new_key.jpg)

![](../images/week01/10_id_rsa.jpg)

On git use the next command to convert image password to text password:
cat ~/.ssh/id_rsa.pub
![](../images/week01/11_convert_graphic_password_to_text_password.jpg)

Copy the text password and paste on git lab "Add SSH Key"
![](../images/week01/12_adding_ssh_password.jpg)

Your SSH Key will registered
![](../images/week01/12A_SSH_KEY.jpg)
Now validate your user with the next commands
pwd
git config --global user.name "user name"
git config --global user.email "user email"
![](../images/week01/13_account_config.jpg)
### 6. Push your uploads to web

Make a git status, to know how is your PC project
![](../images/week01/14_git_status.jpg)

Use the next command
git add .
git commit -m "name of the commit"
git push
you must sign in with your browser
![](../images/week01/15_git_push.jpg)

Authotie your credential manager
![](../images/week01/16_authotize.jpg)

And now every change on your PC project, you can upload on gitlab pushing them
![](../images/week01/17_edit_index.jpg)
![](../images/week01/18_edit_index_with_sublime.jpg)
![](../images/week01/19_save_index.jpg)
![](../images/week01/20_pushing_index.jpg)
![](../images/week01/21_edit_about_index.jpg)
![](../images/week01/22_pushing_about_index.jpg)

Your assignment will be uploaded.
