# 3. Computer Aided design

Now I am trying some 2D and 3D design softwares 

## Trying Corel (2D design software)

It is a powerful tool to 2D design.
<iframe width="640" height="480" src="https://www.youtube.com/embed/s89S4NkNQ5Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This is the design.

![](../files/Week03/KEY_DISTRIBUTION_COREL.jpg)

This is my design in corel.

[Donwload project corel](../files/Week03/PROJECT_COREL.cdr)

## Trying Inkscape (2D design software)

Inksacape is a free 2D design software, it has many tools like corel.

This is an image about Inkscape.

![](../files/Week03/TRYING_INKSCAPE.jpg)

## Trying Fusion 360 (3D design software)

Fusion 360 is a complete tool to 3D designs, it has many tool to create, edit, animate, process to several cnc machines.

Trying fusion 360
<iframe width="560" height="315" src="https://www.youtube.com/embed/c3ZnXn5b3W8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Fusion 360 Design file
[Click here to Download](../files/Week03/Trying_Fusion_360_video.f3z)