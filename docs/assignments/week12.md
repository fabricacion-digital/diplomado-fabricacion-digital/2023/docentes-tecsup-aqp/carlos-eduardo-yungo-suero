# 11. Output devices

This unit I worked on doing work a Servo motor.

## Trying code 

I used a simple code to make work a servo motor.

```
#include <ESP32Servo.h>

Servo servo;
int pinServo=21;

void setup() {
 servo.attach(pinServo);
 servo.write(0);
}

void loop() {
  delay(2000);
  servo.write(90);
  delay(2000);
  servo.write(185);
  delay(2000);
  servo.write(90);
  delay(2000);
  servo.write(0);
}

```

![](../files/week11/servo_yo.jpeg)

![](../files/week11/servo1.mp4)

It is important notice that ESP32 send signal that are interpreted for the component (Servo).

For example Esp32 send different voltages and each voltage make move the servo in different grades. 
In the next images are different voltages for 0º, 45º, 90º, 135º and 180º.

![](../files/week11/89.jpeg)

![](../files/week11/166.jpeg)

![](../files/week11/243.jpeg)

![](../files/week11/320.jpeg)

![](../files/week11/397.jpeg)


## Trying an interface with buttons

Now I designed and made an board to control the servo with buttons.

This is the scheme

![](../files/week11/26.jpg)

The code used was the following.

```
#include <ESP32Servo.h>
//Instanciamos el servomotor
Servo servo;
//Declaramos el pin del pulsador 1
int pinPulsador1 = 19;
//Declaramos el pin del pulsador 2
int pinPulsador2 = 18;
//Declaramos e pin del servo
int pinServo = 21;
void setup()
{
  Serial.begin(115200);
  //Configuramos ESP32 para que resiba la pulsasiones del pulsador 1
  pinMode(pinPulsador1, INPUT);
  //Configuramos ESP32 para que resiba la pulsasiones del pulsador 2
  pinMode(pinPulsador2, INPUT);
  //Configuramos el servo
  servo.attach(pinServo);
  
}
//Inicilizamos la posicion del servo
int posServo = 0;
void loop()
{
  //Si se preciona el pulsador1
  if (digitalRead(pinPulsador1) == HIGH) {
    //Movemos el servo 180 grados
    //servo.write(180);
    servo.write(180);
    delay(2000);
  }
  //Si se preciona el pulsador2
  if (digitalRead(pinPulsador2) == HIGH) {
    //Movemos el servo 0 grados
    //servo.write(0);
    servo.write(0);
    delay(2000);
  }
     //Si se preciona el pulsador2
  if (digitalRead(pinPulsador2) == LOW) {
    //Movemos el servo 0 grados
    //servo.write(0);
    servo.write(90);
    delay(500);

  }
 
}
```

## Interface with buttons

I made a interface and the design to monofab is the following.

[Interface design](../files/week11/SERVO.nc)

[Marco servo](../files/week11/MARCO_SERVO.nc)

And this the result

![](../files/week11/button_board.jpeg)

![](../files/week11/servo_button.jpeg)

![](../files/week11/servo_button_vid.mp4)

