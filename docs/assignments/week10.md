# 7. Molding and casting

## Making a mold with silicone rubber

I did a mold using silicon rubber.

1.- We need  something that retain the silicon, I used acrylic joined with hot silicon.
![](../files/week07/01.jpg)
![](../files/week07/02.jpg)

2.- Using industrial clay, divide the object by half.
![](../files/week07/03.jpg)
![](../files/week07/04.jpg)

3.- With rice we can know how many of silicon rubber we need.
![](../files/week07/05.jpg)

4.- Before use the silicon is important apply mold release.
![](../files/week07/06.jpg)

6.- the silicon that I used need 3% of catalyst by weight.
![](../files/week07/07.jpg)

7.- The silicon must aplied from a corner to avoid bubbles.
![](../files/week07/08.jpg)
![](../files/week07/09.jpg)
![](../files/week07/10.jpg)

8.- Wait 1 day.
![](../files/week07/11.jpg)

9.- Let's remove the clay.
![](../files/week07/12.jpg)
![](../files/week07/13.jpg)
![](../files/week07/14.jpg)

10.- Let's apply mold release again and use silicon to another half.
![](../files/week07/15.jpg)
![](../files/week07/16.jpg)
![](../files/week07/17.jpg)

11.- After a day, it ready to cast it.
![](../files/week07/18.jpg)
![](../files/week07/19.jpg)
![](../files/week07/20.jpg)

## Casting with polyester resin

1.- to prepare the resin I used 15 drops of catalyst by 100 gr of resin.
![](../files/week07/21.jpg)
![](../files/week07/22.jpg)

2.- Let's wait until resin is hard. And it is ready
![](../files/week07/23.jpg)
![](../files/week07/24.jpg)

## Making a mold with monofab.

1.- This is monofab machine
![](../files/week07/a.jpg)

2.- Let's put an affordable bit to the work.
![](../files/week07/b.jpg)

3.- I used wax added with doble face tape.
![](../files/week07/c.jpg)
![](../files/week07/d.jpg)

4.- The design was made in fusion 360, watch the nest videos.
<iframe width="560" height="315" src="https://www.youtube.com/embed/cl5OlXuHLeA?si=c-sQHkyX8P8pcIsq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ScV_t3bPI3Q?si=1vK5vU82bQi_bzJP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

[You can download the design here](../files/week07/CABLE_HOLDER_v2.f3d)

5.- After I used monofab machine to create a negative to the mold.
![](../files/week07/e.jpg)
![](../files/week07/f.jpg)

6.- It is important pause the process to clean the wax because it adds to negative.
![](../files/week07/g.jpg)
![](../files/week07/h.jpg)
![](../files/week07/i.jpg)

