# 10. Electronics production

## Using FlatCam

To produce an electronic board, I export the design from Flux to FlatCam.
BUt I make a test to explore the realtion between FlatCam Soft and MonoFab CNC.

To the Test I made the design in Corel after imported into Flat Cam.
These are my designs to the test

[Archivo png](../files/week10/TEST_MONOFAB.png)

[Archivo dxf](../files/week10/TEST_MONOFAB.dxf)

[Archivo nc para corte en Monofab](../files/week10/TEST_MONOFAB.nc)

After I made the next to export the ellectronic design to monofab.

![](../files/week10/2.jpg)

![](../files/week10/3.jpg)

![](../files/week10/4.jpg)

![](../files/week10/5.jpg)

![](../files/week10/6.jpg)

![](../files/week10/7.jpg)

![](../files/week10/8.jpg)

![](../files/week10/9.jpg)

![](../files/week10/10.jpg)

![](../files/week10/11.jpg)

![](../files/week10/12.jpg)

![](../files/week10/13.jpg)

![](../files/week10/14.jpg)

![](../files/week10/15.jpg)

![](../files/week10/16.jpg)

![](../files/week10/mofab.jpeg)

![](../files/week10/test2.jpeg)

![](../files/week10/test1.jpeg)

Finally made the same process to produce ESP32 board.

![](../files/week10/17.jpg)

![](../files/week10/18.jpg)

![](../files/week10/19.jpg)

![](../files/week10/20.jpg)

![](../files/week10/21.jpg)

![](../files/week10/22.jpg)

![](../files/week10/23.jpg)

![](../files/week10/boar_main1.jpeg)

## Troubles and solutions

A trouble was set the right deep to isolate the ways, then I had review many times before hte board is ready.

## Welding, components on board

To weld the electronic companents on the board I used Solderin iron and tin.

![](../files/week10/board1.jpeg)

The coponents used are in the next link
[Flux project](https://www.flux.ai/carlosyungo/esp32-board?editor=schematic)