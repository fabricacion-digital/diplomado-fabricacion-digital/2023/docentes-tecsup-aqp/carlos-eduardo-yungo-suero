# 8. Embedded programming

For this sesion we korked in embedded programing using ARDUINO IDE as software and two DevKits (ARDUINO MEGA and ESP32)

## ARDUINO MEGA VS ESP32 DevKit

ARDUINO MEGA
![](../files/week08/mega.jpg)  

ESP32 DEVKIT-C
![](../files/week08/esp32.jpg)

### Microcontroller:

Arduino Mega: It uses the ATmega2560 microcontroller, based on the AVR architecture.
ESP32 DevKit C: It is equipped with the ESP32 microcontroller, which is a dual-core Xtensa 32-bit LX6 processor.
Processor Speed:

Arduino Mega: Typically runs at 16 MHz.
ESP32 DevKit C: The ESP32 usually runs at higher speeds, with a dual-core processor capable of reaching up to 240 MHz.

### Memory:

Arduino Mega: Has 256 KB of flash memory and 8 KB of SRAM.
ESP32 DevKit C: Comes with a varying amount of flash memory (typically 4MB or more) and 520 KB of SRAM.
Wireless Connectivity:

Arduino Mega: Generally lacks built-in wireless connectivity.
ESP32 DevKit C: Has built-in Wi-Fi and Bluetooth capabilities, making it suitable for IoT applications.

### GPIO Pins:

Arduino Mega: Offers a larger number of digital and analog pins (54 digital pins and 16 analog inputs).
ESP32 DevKit C: Provides a respectable number of GPIO pins, but usually fewer than the Mega (e.g., 36 GPIO pins on some models).
Analog-to-Digital Converter (ADC):

Arduino Mega: Features a 10-bit ADC.
ESP32 DevKit C: Usually comes with a 12-bit ADC, providing higher precision for analog inputs.

### USB Connectivity:

Arduino Mega: Utilizes a USB-B connector for communication.
ESP32 DevKit C: Also features a micro-USB connector for communication and power.
Programming Environment:

Arduino Mega: Typically programmed using the Arduino IDE and the Arduino programming language.
ESP32 DevKit C: Can be programmed using the Arduino IDE, but is often programmed using the Espressif IoT Development Framework (ESP-IDF) or the Arduino framework for ESP32.

### Power Consumption:

Arduino Mega: Generally consumes more power.
ESP32 DevKit C: Designed for lower power consumption, making it suitable for battery-powered applications.

### Cost:

Arduino Mega: Generally, Arduino Mega boards are less expensive.
ESP32 DevKit C: May be slightly more expensive, but the added features like Wi-Fi and Bluetooth justify the cost for certain applications.

In summary, the choice between the Arduino Mega and ESP32 DevKit C depends on your specific project requirements. If you need more GPIO pins and are not concerned about wireless connectivity, the Arduino Mega might be sufficient. However, if you require features like Wi-Fi, Bluetooth, higher processing speed, and lower power consumption, the ESP32 DevKit C would be a better choice.


## Installing ARDUINO IDE

To install ARDUINO IDE software you can donwload it from:

- [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software)

![](../files/week08/0.jpg)

![](../files/week08/1.jpg)

### Installing ARDUINO IDE to ESP32

To set up ARDUINO to ESP32 follow the next steps:

![](../files/week08/1a.jpg)

Copy this link

![](../files/week08/1b.jpg)

Paste in "Aditional boards manager URLs".

![](../files/week08/1c.jpg)

![](../files/week08/2.jpg)

![](../files/week08/3.jpg)

Install COM Driver.

![](../files/week08/4.jpg)

![](../files/week08/5.jpg)

![](../files/week08/6.jpg)

![](../files/week08/7.jpg)

## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

## Embedding code in ESP32

Coding in Arduino IDE

![](../files/week08/8.jpg)

Set the led

![](../files/week08/FOTO_ESP32.jpeg)

Video

![](../files/week08/vIDEO_ESP32.mp4)

## Embedding code in MEGA

Coding in Arduino IDE

![](../files/week08/9.jpg)

Set the led

![](../files/week08/FOTO_MEGA.jpeg)

Video

![](../files/week08/vIDEO_MEGA.mp4)

![](../files/week08/FOTO_YO.jpeg)

