# 5. 3D Scanning and printing

## 3D printing

I used two models of printer:
- Creality CR20Pro

### Parameterizing 3D printer

Is important to know the gaps that 3D printer generate at prints.
That's why you must print test.
I obtained my test print from:

[link](https://www.thingiverse.com/thing:2011862)
![](../files/week05/print_test.jpg)

With the print test I noticed that there are gaps between the design and the 3Dprint.

For example the 3D print has more external diameter that the design.

![](../files/week05/external_diameter.jpg)

And in the internal diameter also there is a gap.

![](../files/week05/internal_diameter.jpg)

We need to consider this gaps to design our components.

### 3D print

First we need to use an slicer software to process the 3D object.
I used Ultimeker Cura.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ErdF9SV_UCI?si=QyMbBCmf1J6x9NhU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This is a video showing how print with Creality CR20Pro.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Lc6fDA4txM8?si=d1HMD4Kjo-pZYikP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

And this video shows how change the filament.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TP-tmJFG4Js?si=8coymtZOF1pjrFkJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## 3D Scan

To 3D Scanning I used a "Sense" 3D Scan.

![](../files/week05/sense_1.jpg)
![](../files/week05/sense_2.jpg)
![](../files/week05/sense_3.jpg)

In this video show the process to scanning 3D.
This process was made repeatly because the 3D sacn need to be aligned and with a constant movement.

Part 1

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hii-gxtOO_s?si=3l5OD9NbbLRErkqV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Part 2

<iframe width="560" height="315" src="https://www.youtube.com/embed/NbujDZCVFLQ?si=0JT9KQ5jxi-CJ8Nm" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Finally I print myself.

![](../files/week05/myself.jpg)







